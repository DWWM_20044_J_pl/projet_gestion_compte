<?php echo PHP_EOL;

# Copyright (C) 2020, camille.silverberg@gmail.com
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

include 'nardb.php';

$bank = \dwwm\nardb\create([
	'branchs' => [
		[
			'__primary_key' => [
				'identifier',
				'Identifiant de l’agence',
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
			],
			'name' => [
				'text',
				'Nom de l’agence',
			],
			'address' => [
				'text',
				'Adresse de l’agence',
			],
		],
		[
			['name', 'address', ],
		],
	],
	'natural_persons' => [
		[
			'__primary_key' => [
				'identifier',
				'Identifiant du client',
				['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ],
				['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
			],
			'name' => [
				'text',
				'Nom du client',
			],
			'surname' => [
				'text',
				'Prenom du client',
			],
			'birthdate' => [
				'date',
				'Date de naissance du client',
				'Y-m-d',
			],
			'email_address' => [
				'text',
				'Email du client',
			],
		],
		[
			['name', 'surname', 'birthdate', 'email_address', ],
		],
	],
	'accounts' => [
		[
			'__primary_key' => [
				'identifier',
				'Identifiant du compte',
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ],
			],
			'natural_person' => [
				'reference',
				'natural_persons',
			],
			'branch' => [
				'reference',
				'branchs',
			],
			'type' => [
				'enumerated',
				'Type du compte',
				'compte courant',
				'livret a',
				'plan epargne logement',
			],
			'balance' => [
				'integer',
				'Solde du compte',
			],
			'can_overdraw' => [
				'boolean',
				'Decouvert autorise',
			],
		],
		[
			['natural_person', 'type', ],
		],
	],
]);

\dwwm\nardb\initialize($bank);

function pretty_print_natural_person($bank, $natural_person_identifier) {
	$natural_person = current($bank['natural_persons']['select'](['__primary_key' => $natural_person_identifier, ]));
	$accounts = $bank['accounts']['select'](['natural_person' => $natural_person_identifier, ]);
	
	$pretty_print = '';
	
	$pretty_print .= 'Identifiant: '. $bank['natural_persons']['columns']['__primary_key']['serialize']($natural_person['__primary_key']) .PHP_EOL;
	$pretty_print .= 'Nom: '. $bank['natural_persons']['columns']['name']['serialize']($natural_person['name']) .PHP_EOL;
	$pretty_print .= 'Prenom: '. $bank['natural_persons']['columns']['surname']['serialize']($natural_person['surname']) .PHP_EOL;
	$pretty_print .= 'Date de naissance: '. $bank['natural_persons']['columns']['birthdate']['serialize']($natural_person['birthdate']) .PHP_EOL;
	$pretty_print .= '---' .PHP_EOL;
	
	$pretty_print .= 'Liste de compte' .PHP_EOL;
	$pretty_print .= '---' .PHP_EOL;
	
	$pretty_print .= implode("\011", ['Numero de compte', 'Solde', ]) .PHP_EOL;
	$pretty_print .= '---' .PHP_EOL;
	
	foreach ($accounts as $account) {
		$pretty_print .= $bank['accounts']['columns']['__primary_key']['serialize']($account['__primary_key']) ."\011";
		$pretty_print .= number_format($account['balance']/100, 2) .'euros' ."\011";
		if ($account['balance'] < 0) {
			$pretty_print .= ':-(' .PHP_EOL;
		} else {
			$pretty_print .= ':-)' .PHP_EOL;
		}
	}
	$pretty_print .= '---' .PHP_EOL;
	
	return $pretty_print;
}
function save_natural_person_details($bank) {
	$natural_person_identifier = $bank['natural_persons']['columns']['__primary_key']['interactive_insertion']();
	$natural_persons = $bank['natural_persons']['select'](['__primary_key' => $natural_person_identifier, ]);
	
	if (empty($natural_persons)) {
		echo 'Aucun client trouve pour cet identifiant.' .PHP_EOL;
		return FALSE;
	}
	
	file_put_contents('filename.txt', pretty_print_natural_person($bank, $natural_person_identifier));
	echo 'Informations du client ecrites.' .PHP_EOL;
	return TRUE;
}

$user_did_exit = FALSE;
while (FALSE === $user_did_exit):
	echo '------------[ BANQUE DWWM 20044: MENU PRINCIPAL ]-----------------------' .PHP_EOL;
	echo "\011". 'A ― Creer une agence' .PHP_EOL;
	echo "\011". 'B ― Creer un client' .PHP_EOL;
	echo "\011". 'C ― Creer un compte' .PHP_EOL;
	echo "\011". 'D ― Chercher une agence' .PHP_EOL;
	echo "\011". 'E ― Chercher un client' .PHP_EOL;
	echo "\011". 'F ― Chercher un compte' .PHP_EOL;
	echo "\011". 'G ― Afficher les comptes d’un client' .PHP_EOL;
	echo "\011". 'H ― Enregistrer les comptes d’un client' .PHP_EOL;
	echo "\011". 'Q ― Quitter' .PHP_EOL;
	echo "\011". 'S ― Source et licence' .PHP_EOL;
	echo '------------------------------------------------------------------------' .PHP_EOL;
	
	$choice = readline('Votre choix: ');
	if ('A' === $choice):
		$bank['branchs']['interactive_insertion']();
	elseif ('B' === $choice):
		$bank['natural_persons']['interactive_insertion']();
	elseif ('C' === $choice):
		$bank['accounts']['interactive_insertion']();
	elseif ('D' === $choice):
		$bank['branchs']['interactive_selection']();
	elseif ('E' === $choice):
		$bank['natural_persons']['interactive_selection']();
	elseif ('F' === $choice):
		$bank['accounts']['interactive_selection']();
	elseif ('G' === $choice):
		$bank['accounts']['interactive_selection_on_single_column']('natural_person');
	elseif ('H' === $choice):
		save_natural_person_details($bank);
	elseif ('Q' === $choice):
		echo 'aurevoir' .PHP_EOL;
		$user_did_exit = TRUE;
	elseif ('S' === $choice):
		echo 'Ce programme est distribue sous licence GNU AGPLv3.' .PHP_EOL;
		echo 'Son code source est disponible sur le lien suivant:' .PHP_EOL;
		echo '<https://bitbucket.org/DWWM_20044_J_pl/projet_gestion_compte/>' .PHP_EOL;
	else:
		echo 'choix non reconnu' .PHP_EOL;
	endif;
endwhile;
