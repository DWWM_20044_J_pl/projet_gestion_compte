<?php

# Copyright (C) 2020, camille.silverberg@gmail.com
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

namespace dwwm\nardb {

function create($table_definitions) {
	$tables = [];
	foreach ($table_definitions as $table_name => $table_definition) {
		$tables[$table_name] = \dwwm\nardb\table\create($tables, $table_name, $table_definition);
	}
	return $tables;
}

function initialize($tables) {
	foreach ($tables as $table_name => $table) {
		\dwwm\nardb\table\initialize($table, $table_name, $tables);
	}
}

}


namespace dwwm\nardb\table {

function associated_csv_filename($table_name) {
	return dirname(__FILE__) .'/'. $table_name .'.csv';
}

function create($tables, $table_name, $table_definition) {
	list($column_definitions, $unique_constraints) = $table_definition;
	
	$columns = [];
	foreach ($column_definitions as $column_name => $column_definition) {
		$columns[$column_name] = \dwwm\nardb\column\create($tables, $table_name, $column_name, $column_definition);
	}
	
	$rows = [];
	$select = function($search) use (&$rows) {
		$selected = $rows;
		foreach ($search as $column_name => $column_value) {
			$selected = array_filter($selected, fn($row) => $row[$column_name] === $column_value);
		}
		return $selected;
	};
	$commit = function() use (&$rows, $table_name, $columns) {
		$handle = fopen(associated_csv_filename($table_name), 'w');
		foreach ($rows as $row) {
			$fields = [];
			
			foreach ($columns as $column_name => $column) {
				$fields[] = $column['serialize']($row[$column_name]);
			}
			
			fputcsv($handle , $fields);
		}
		fclose($handle);
	};
	$insert = function($row) use (&$rows, $select, $commit, $unique_constraints) {
		$selected = $select(['__primary_key' => $row['__primary_key'], ]);
		if (FALSE === empty($selected)) {
			return 'Cette cle primaire est deja utilisee.';
		}
		
		foreach ($unique_constraints as $unique_constraint) {
			$selected = $select(array_combine($unique_constraint, array_map(fn($column_name) => $row[$column_name], $unique_constraint)));
			if (FALSE === empty($selected)) {
				return 'Cette ligne n’est pas unique.';
			}
		}
		
		$rows[] = $row;
		$commit();
		return NULL;
	};
	
	$display_rows = function($rows) use ($columns) {
		echo 'Lignes trouvees:' .PHP_EOL;
		foreach (array_values($rows) as $index => $row) {
			echo strval($index+1) .' —';
			foreach ($row as $column_name => $value) {
				echo "\011". $columns[$column_name]['namevalue_as_string']($value) .PHP_EOL;
			}
		}
	};
	$interactive_selection = function() use ($columns, $select, $display_rows) {
		$search = [];
		
		foreach ($columns as $column_name => $column) {
			$search[$column_name] = $column['interactive_selection']();
		}
		
		$search = array_filter($search, fn($value) => NULL !== $value);
		if (empty($search)) {
			echo 'Recherche invalide.' .PHP_EOL;
			return FALSE;
		}
		
		$selected = $select($search);
		if (empty($selected)) {
			echo 'Aucune ligne trouvee.' .PHP_EOL;
			return FALSE;
		}
		
		$display_rows($selected);
		return TRUE;
	};
	$interactive_selection_on_single_column = function($column_name) use ($columns, $select, $display_rows) {
		$search = [];
		
		$search[$column_name] = $columns[$column_name]['interactive_selection']();
		
		if (NULL === $search[$column_name]) {
			echo 'Recherche invalide.' .PHP_EOL;
			return FALSE;
		}
		
		$selected = $select($search);
		if (empty($selected)) {
			echo 'Aucune ligne trouvee.' .PHP_EOL;
			return FALSE;
		}
		
		$display_rows($selected);
		return TRUE;
	};
	$interactive_insertion = function() use ($columns, $insert) {
		$row = [];
		
		foreach ($columns as $column_name => $column) {
			$row[$column_name] = $column['interactive_insertion']();
		}
		
		$error = $insert($row);
		if (NULL !== $error) {
			echo $error .PHP_EOL;
			return FALSE;
		}
		
		echo 'Ligne inseree.' .PHP_EOL;
		return TRUE;
	};
	
	return [
		'columns' => $columns,
		'select' => $select,
		'insert' => $insert,
		'interactive_selection' => $interactive_selection,
		'interactive_selection_on_single_column' => $interactive_selection_on_single_column,
		'interactive_insertion' => $interactive_insertion,
	];
}

function initialize($table, $table_name, $tables) {
	if (FALSE === file_exists(associated_csv_filename($table_name))) {
		return;
	}
	
	if (FALSE !== ($handle = fopen(associated_csv_filename($table_name), 'r'))) {
		while (FALSE !== ($fields = fgetcsv($handle))) {
			$row = [];
			
			foreach ($table['columns'] as $column_name => $column) {
				$row[$column_name] = $column['deserialize'](current($fields));
				next($fields);
			}
			
			$table['insert']($row);
		}
		fclose($handle);
	}
}

}


namespace dwwm\nardb\column {

function create($tables, $table_name, $column_name, $column_definition) {
	$column_type = $column_definition[0];
	if ('boolean' === $column_type) {
		return boolean($tables, $table_name, $column_name, array_slice($column_definition, 1));
	} elseif ('date' === $column_type) {
		return date($tables, $table_name, $column_name, array_slice($column_definition, 1));
	} elseif ('enumerated' === $column_type) {
		return enumerated($tables, $table_name, $column_name, array_slice($column_definition, 1));
	} elseif ('identifier' === $column_type) {
		return identifier($tables, $table_name, $column_name, array_slice($column_definition, 1));
	} elseif ('integer' === $column_type) {
		return integer($tables, $table_name, $column_name, array_slice($column_definition, 1));
	} elseif ('reference' === $column_type) {
		return reference($tables, $table_name, $column_name, array_slice($column_definition, 1));
	} elseif ('text' === $column_type) {
		return text($tables, $table_name, $column_name, array_slice($column_definition, 1));
	}
}

function boolean($tables, $table_name, $column_name, $column_definition) {
	$human_friendly_name = $column_definition[0];
	
	$truthy = ['1', 'oui', 'o', 'true', 't', 'yes', 'y', 'on', '⊤', ];
	$falsy = ['0', 'non', 'n', 'false', 'f', 'no', 'off', '⊥', ];
	
	$serialize = function($value) {
		if ($value) {
			return '1';
		} else {
			return '0';
		}
	};
	$deserialize = function($string) {
		return $string === '1';
	};
	
	$namevalue_as_string = function($value) use ($human_friendly_name, $truthy, $falsy) {
		if ($value) {
			echo $human_friendly_name .': '. $truthy[1];
		} else {
			echo $human_friendly_name .': '. $falsy[1];
		}
	};
	$interactive_selection = function() use ($human_friendly_name, $truthy, $falsy) {
		while (FALSE === empty($line = readline($human_friendly_name .': '))) {
			if (in_array($line, $truthy, TRUE)) {
				return TRUE;
			}
			if (in_array($line, $falsy, TRUE)) {
				return FALSE;
			}
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return NULL;
	};
	$interactive_insertion = function() use ($interactive_selection) {
		while (NULL === ($value = $interactive_selection())) {
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return $value;
	};
	
	return [
		'deserialize' => $deserialize,
		'serialize' => $serialize,
		'namevalue_as_string' => $namevalue_as_string,
		'interactive_selection' => $interactive_selection,
		'interactive_insertion' => $interactive_insertion,
	];
}
function date($tables, $table_name, $column_name, $column_definition) {
	$human_friendly_name = $column_definition[0];
	$format = $column_definition[1];
	
	$serialize = function($value) {
		return strval($value);
	};
	$deserialize = function($string) {
		return intval($string);
	};
	
	$namevalue_as_string = function($value) use ($human_friendly_name, $format) {
		return $human_friendly_name .': '. (new \DateTimeImmutable('now', new \DateTimeZone('UTC')))->setTimestamp($value)->format($format);
	};
	$interactive_selection = function() use ($human_friendly_name, $format) {
		while (FALSE === empty($line = readline($human_friendly_name .': '))) {
			if (FALSE !== ($value = \DateTimeImmutable::createFromFormat($format, $line, new \DateTimeZone('UTC')))) {
				return $value->setTime(0, 0)->getTimestamp();
			}
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return NULL;
	};
	$interactive_insertion = function() use ($interactive_selection) {
		while (NULL === ($value = $interactive_selection())) {
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return $value;
	};
	
	return [
		'deserialize' => $deserialize,
		'serialize' => $serialize,
		'namevalue_as_string' => $namevalue_as_string,
		'interactive_selection' => $interactive_selection,
		'interactive_insertion' => $interactive_insertion,
	];
}
function enumerated($tables, $table_name, $column_name, $column_definition) {
	$human_friendly_name = $column_definition[0];
	$options = array_slice($column_definition, 1);
	
	$serialize = function($value) {
		return strval($value);
	};
	$deserialize = function($string) {
		return intval($string);
	};
	
	$namevalue_as_string = function($value) use ($human_friendly_name, $options) {
		return $human_friendly_name .': '. $options[$value];
	};
	$interactive_selection = function() use ($human_friendly_name, $options) {
		while (FALSE === empty($line = readline($human_friendly_name .': '))) {
			if (FALSE !== ($value = array_search($line, $options, TRUE))) {
				return $value;
			}
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return NULL;
	};
	$interactive_insertion = function() use ($interactive_selection) {
		while (NULL === ($value = $interactive_selection())) {
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return $value;
	};
	
	return [
		'deserialize' => $deserialize,
		'serialize' => $serialize,
		'namevalue_as_string' => $namevalue_as_string,
		'interactive_selection' => $interactive_selection,
		'interactive_insertion' => $interactive_insertion,
	];
}
function identifier($tables, $table_name, $column_name, $column_definition) {
	$human_friendly_name = $column_definition[0];
	$alphabets = array_slice($column_definition, 1);
	
	$generate_random_value = function() use ($alphabets) {
		$value = [];
		foreach ($alphabets as $alphabet) {
			$value[] = random_int(0, count($alphabet) -1);
		}
		return $value;
	};
	$deserialize = function($string) use ($alphabets) {
		$value = [];
		foreach ($alphabets as $index => $alphabet) {
			$index = array_search($string[$index], $alphabet, TRUE);
			if (FALSE === $index) {
				return FALSE;
			}
			$value[] = $index;
		}
		return $value;
	};
	$serialize = function($value) use ($alphabets) {
		$string = '';
		foreach ($alphabets as $index => $alphabet) {
			$string .= $alphabet[$value[$index]];
		}
		return $string;
	};
	
	$namevalue_as_string = function($value) use ($human_friendly_name, $serialize) {
		return $human_friendly_name .': '. $serialize($value);
	};
	$interactive_selection = function($default_value=FALSE) use ($human_friendly_name, $serialize, $deserialize) {
		if (FALSE === $default_value) {
			$prompt = $human_friendly_name .': ';
			while (FALSE === empty($line = readline($prompt))) {
				if (FALSE !== ($value = $deserialize($line))) {
					return $value;
				}
				echo 'Saisie invalide, recommencez.' .PHP_EOL;
			}
			return NULL;
		} else {
			$prompt = $human_friendly_name .' ('. $serialize($default_value) .'): ';
			while (FALSE === empty($line = readline($prompt))) {
				if (FALSE !== ($value = $deserialize($line))) {
					return $value;
				}
				echo 'Saisie invalide, recommencez.' .PHP_EOL;
			}
			return $default_value;
		}
	};
	$interactive_insertion = function($default_value=TRUE) use ($interactive_selection, $generate_random_value) {
		if (TRUE === $default_value) {
			$default_value = $generate_random_value();
		}
		while (NULL === ($value = $interactive_selection($default_value))) {
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return $value;
	};
	
	return [
		'deserialize' => $deserialize,
		'serialize' => $serialize,
		'namevalue_as_string' => $namevalue_as_string,
		'interactive_selection' => $interactive_selection,
		'interactive_insertion' => $interactive_insertion,
	];
}
function integer($tables, $table_name, $column_name, $column_definition) {
	$human_friendly_name = $column_definition[0];
	
	$serialize = function($value) {
		return strval($value);
	};
	$deserialize = function($string) {
		return intval($string);
	};
	
	$namevalue_as_string = function($value) use ($human_friendly_name) {
		return $human_friendly_name .': '. strval($value);
	};
	$interactive_selection = function() use ($human_friendly_name) {
		while (FALSE === empty($line = readline($human_friendly_name .': '))) {
			if (strval(intval($line)) === $line) {
				return intval($line);
			}
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return NULL;
	};
	$interactive_insertion = function() use ($interactive_selection) {
		while (NULL === ($value = $interactive_selection())) {
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return $value;
	};
	
	return [
		'deserialize' => $deserialize,
		'serialize' => $serialize,
		'namevalue_as_string' => $namevalue_as_string,
		'interactive_selection' => $interactive_selection,
		'interactive_insertion' => $interactive_insertion,
	];
}
function reference($tables, $table_name, $column_name, $column_definition) {
	$column = $tables[$column_definition[0]]['columns']['__primary_key'];
	$column['interactive_insertion'] = fn() => $column['interactive_insertion'](FALSE);
	return $column;
}
function text($tables, $table_name, $column_name, $column_definition) {
	$human_friendly_name = $column_definition[0];
	
	$serialize = function($value) {
		return $value;
	};
	$deserialize = function($string) {
		return $string;
	};
	
	$namevalue_as_string = function($value) use ($human_friendly_name) {
		return $human_friendly_name .': '. $value;
	};
	$interactive_selection = function() use ($human_friendly_name) {
		while (FALSE === empty($line = readline($human_friendly_name .': '))) {
			return $line;
		}
		return NULL;
	};
	$interactive_insertion = function() use ($interactive_selection) {
		while (NULL === ($value = $interactive_selection())) {
			echo 'Saisie invalide, recommencez.' .PHP_EOL;
		}
		return $value;
	};
	
	return [
		'deserialize' => $deserialize,
		'serialize' => $serialize,
		'namevalue_as_string' => $namevalue_as_string,
		'interactive_selection' => $interactive_selection,
		'interactive_insertion' => $interactive_insertion,
	];
}

}